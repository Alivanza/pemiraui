from django.urls import path

from . import views

app_name = 'mulai'

urlpatterns = [
    path('', views.index, name='index'),
]